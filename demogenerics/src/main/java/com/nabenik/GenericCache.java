package com.nabenik;

public class GenericCache <T> {

    private T cacheElement;

    public void setCache(T cacheElement) {
        this.cacheElement = cacheElement;
    }

    public T getCache() {
        return this.cacheElement;
    }

}