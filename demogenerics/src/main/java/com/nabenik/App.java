package com.nabenik;

import java.util.List;
import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        List<Integer> lista = new ArrayList<>();
        lista.add(100);
        lista.add(200);
        //lista.add("Hola mundo");

        for (var elemento : lista) {
            int numero = (int) elemento;
            System.out.println(numero + 1);
        }

    }
}
