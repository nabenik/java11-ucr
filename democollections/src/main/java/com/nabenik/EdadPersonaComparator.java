package com.nabenik;

import java.util.Comparator;

public class EdadPersonaComparator implements Comparator<Persona>{

    @Override
    public int compare(Persona p1, Persona p2){
        return p1.getAge().compareTo(p2.getAge());
    }
    
}