package com.nabenik;

//toString, equals, hashCode, encapsulamiento, inmutable
// C#, Kotlin, Scala, TypeScript - Data Carrier
public record Persona2 (String name,
    Integer age, Boolean quiet) {}