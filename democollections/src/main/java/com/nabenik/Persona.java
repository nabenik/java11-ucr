package com.nabenik;

/**
 * Plain old Java Object
 * Data Carriers
 * Reglas - 1. Convenciones de nombrado de Java
 * 2. Propiedades encapsuladas
 * 3. Constructor por defecto implicito o explicito
 */
public class Persona implements Comparable<Persona> {
    
    private String name;
    private Integer age;
    private Boolean quiet;

    public Persona(){
        
    }

    public Persona(String name, Integer age, Boolean quiet) {
        this.name = name;
        this.age = age;
        this.quiet = quiet;
    }
    //to string
    //hashCode - Object - Direccion de memoria

    //Equals - Object - Direccion de memoria

    public Boolean isQuiet(){
        return this.quiet;
    }

    public void setQuit(Boolean quiet){
        this.quiet = quiet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Persona [age=" + age + ", name=" + name + ", quiet=" + quiet + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((age == null) ? 0 : age.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((quiet == null) ? 0 : quiet.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Persona other = (Persona) obj;
        if (age == null) {
            if (other.age != null)
                return false;
        } else if (!age.equals(other.age))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (quiet == null) {
            if (other.quiet != null)
                return false;
        } else if (!quiet.equals(other.quiet))
            return false;
        return true;
    }

    @Override
    public int compareTo(Persona other){
        return this.name.compareTo(other.getName());
    }


}