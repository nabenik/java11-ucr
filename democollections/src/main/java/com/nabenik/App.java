package com.nabenik;

import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Map<Integer, Persona> personList = new HashMap<>();

        personList.put(1, new Persona("Victor", 32, true));
        personList.put(2, new Persona("Tux", 30, false));
        personList.put(3, new Persona("Duke", 45, false));

        System.out.println(personList.get(2));

        /*Collections.sort(personList, new EdadPersonaComparator());
        System.out.println(personList);

        Collections.sort(personList,
            (p1,p2) -> p1.getAge().compareTo(p2.getAge()));
        System.out.println(personList);*/
    }
}
