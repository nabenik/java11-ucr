package com.nabenik;

public class Automovil {

    private String marca;
    private String modelo;

    public Automovil(String marca, String modelo){
        this.marca = marca;
        this.modelo = modelo;
    }

    public static void hacerRuido(String nombre){
        System.out.println("Hola " + nombre + " estoy haciendo ruido");
    }

    public String avanzar(String nombre){
        return "Hola " + nombre + " soy el auto " + marca + " y estoy avanzando";
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setMarca(String marca){
        this.marca = marca;
    }

    public void setModelo(String modelo){
        this.modelo = modelo;
    }
    
}