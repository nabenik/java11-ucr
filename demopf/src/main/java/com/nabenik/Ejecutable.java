package com.nabenik;

//Una interfaz funcional es el contrato del 'unico' metodo incluido
//en la interfaz
@FunctionalInterface
public interface Ejecutable {

    void crearSaludo(String nombre);

    //Cantidad de argumentos, tipo de dato de los argumentos
    //tipo de dato de retorno
    
    public default void saludar(){
        System.out.println("Hola desde interfaz");
    }
}