package com.nabenik;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Ejecutable referenciaAlmetodo = Automovil::hacerRuido;
        ejecutarComportamiento(referenciaAlmetodo);
    }

    //Funciones - Comportamientos como argumentos (High-order)
    public static void ejecutarComportamiento(Ejecutable ej){
        ej.crearSaludo("UCR");
    }
}
