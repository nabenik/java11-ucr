# Build
mvn clean package && docker build -t com.nabenik/demo-ddd .

# RUN

docker rm -f demo-ddd || true && docker run -d -p 8080:8080 -p 4848:4848 --name demo-ddd com.nabenik/demo-ddd 

# System Test

Switch to the "-st" module and perform:

mvn compile failsafe:integration-test