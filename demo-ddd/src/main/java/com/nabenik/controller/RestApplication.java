
package com.nabenik.controller;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Activator class
 * @author tuxtor
 */
@ApplicationPath("/rest") 
public class RestApplication extends Application {
    
}
