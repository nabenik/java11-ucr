package com.nabenik.controller;

import com.nabenik.model.Phrase;
import com.nabenik.repository.PhraseRepository;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

@Path("phrases")
public class PhraseController {
    
    @EJB
    PhraseRepository phraseRepository;
    
    @GET
    public List<Phrase> listAll(){
        return phraseRepository.listAll();
    }
    
    @GET
    @Path("/{id:[0-9][0-9]*}")
    public Phrase findById(@PathParam("id") Long idPhrase){
        return phraseRepository.findById(idPhrase);
    }
    //Post - crear, put- crear/actualizar
    @POST
    public Response create(Phrase phrase){
        phraseRepository.create(phrase);
        return Response.created(
                UriBuilder.fromResource(this.getClass())
                    .path(phrase.getPhraseId().toString()).build()).build();
    }
    
    @PUT
    @Path("/{id:[0-9][0-9]*}")
    public Response update(@PathParam("id") Long idPhrase, Phrase phrase){
        
        phrase = phraseRepository.update(phrase);
        return Response.ok(phrase).build();
    }
    
    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    public Response delete(@PathParam("id") Long idPhrase){
        phraseRepository.delete(idPhrase);
        return Response.ok().build();
    }
}
