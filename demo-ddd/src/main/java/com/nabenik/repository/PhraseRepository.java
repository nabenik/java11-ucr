package com.nabenik.repository;

import com.nabenik.model.Phrase;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tuxtor
 */
@Stateless
public class PhraseRepository {
    // 1- EntityManager
    @PersistenceContext(name = "ddd-PU")
    EntityManager em;
    
    // 2- Metodos CRUD
    public void create(Phrase phrase){
        em.persist(phrase);
    }
    
    public Phrase update(Phrase phrase){
        return em.merge(phrase);
    }
    
    public void delete(Long idPhrase){
        Phrase toDelete = this.findById(idPhrase);
        em.remove(toDelete);
    }
    
    public Phrase findById(Long idPhrase){
        return em.find(Phrase.class, idPhrase);
    }
    
    public List<Phrase> listAll(){
        //1- SQL Nativo, 2- JPQL Nativo, 3- Criteria Query
        String query = "SELECT p FROM Phrase p";
        
        return em.createQuery(query, Phrase.class)
                .getResultList();
    }
    
}
