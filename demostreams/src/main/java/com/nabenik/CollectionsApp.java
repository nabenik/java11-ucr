package com.nabenik;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class CollectionsApp {

    public static void lambdaPersonList() {
        List<ComparablePerson> personList = new ArrayList<>();
        personList.add(new ComparablePerson("Yoda", 900));
        personList.add(new ComparablePerson("Victor", 32));
        personList.add(new ComparablePerson("Matusalem", 969));

        //Collections.sort(personList);
        personList = personList.stream()
                        //.sorted(ComparablePerson::compareTo)
                        //.sorted(Collections.reverseOrder())
                        .sorted( (a,b) -> a.getAge().compareTo(b.getAge()))
                        .collect(Collectors.toList());
        Collections.sort(personList, ComparablePerson::compareTo);

        for(var person:personList) {
            System.out.println(person.getName());
        }   
    }

    public static void sortPersonList() {
        var personList = new ArrayList<ComparablePerson>();
        personList.add(new ComparablePerson("Yoda", 900));
        personList.add(new ComparablePerson("Victor", 32));
        personList.add(new ComparablePerson("Matusalem", 969));

        //Collections.sort(personList);
        //Collections.sort(personList, ComparablePerson::compareTo);
        //Collections.sort(personList, Collections.reverseOrder());
        Collections.sort(personList, new Comparator<ComparablePerson>(){
            public int compare(ComparablePerson l1, ComparablePerson l2){
                return l1.getAge().compareTo(l2.getAge());
            }
        });

        for(ComparablePerson person:personList) {
            System.out.println(person.getName());
        }   
    }

    public static void main(String args[]) {
        sortPersonList();
        /*Set<ComparablePerson> personList = new TreeSet<>();
        personList.add(new ComparablePerson("Yoda", 900));
        personList.add(new ComparablePerson("Victor", 32));
        personList.add(new ComparablePerson("Matusalem", 969));

        for(ComparablePerson person:personList) {
            System.out.println(person.getName());
        }*/
    }
}