package com.nabenik;

import java.util.List;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class App 
{

    public static List<Language> generateLanguages(){
        List<Language> languagesList = new ArrayList<>();

        languagesList.add(new Language("Java", "JVM"));
        languagesList.add(new Language("Scala", "JVM"));
        languagesList.add(new Language("Kotlin", "JVM"));
        languagesList.add(new Language("JavaScript", "V8/Node"));
        languagesList.add(new Language("TypeScript", "V8/Node"));
        languagesList.add(new Language("Python", "CPython"));
        languagesList.add(new Language("Rust", "Native"));
        languagesList.add(new Language("Go", "Native"));
        return languagesList;
    }

    public static List<Language> doClassicFiltering(List<Language> languages){
        var filteredList = new ArrayList<Language>();

        //for(var language: languages){
        for(int i=0; i < languages.size(); i++) {
            Language language = languages.get(i);
            if(language.getPlatform().equals("JVM")){
                filteredList.add(language);
            }
        }
        return filteredList;
    }

    public static List<Language> doFunctionalFiltering(List<Language> languages){

        return languages.stream()
            .filter(language -> language.getPlatform().equals("JVM"))
            .collect(Collectors.toList());

    }

    public static List<Language> doClassicSort(List<Language> languages){

        Collections.sort(languages, new Comparator<Language>(){
            public int compare(Language l1, Language l2){
                return l1.getName().compareTo(l2.getName());
            }
        });

        return languages;
    }

    public static List<Language> doLambdaSort(List <Language> languages){
        languages
                .sort( (l1, l2) -> l1.getName().compareTo(l2.getName()) );
        return languages;
    }
    public static void main( String[] args )
    {
        var languages = generateLanguages();
        System.out.println(doLambdaSort(languages));
    }
}
