package com.nabenik;

public class Language {
    private String name;
    private String platform;

    public Language(String name, String platform){
        this.name = name;
        this.platform = platform;
    }

    public String toString(){
        return "[" + this.name + "-" + this.platform + "]";
    }


    public void setName(String name){
        this.name = name;
    }

    public void setPlatform(String platform){
        this.platform = platform;
    }

    public String getName(){
        return this.name;
    }

    public String getPlatform(){
        return this.platform;
    }
}