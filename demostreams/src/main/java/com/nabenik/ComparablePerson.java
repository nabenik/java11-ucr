package com.nabenik;

public class ComparablePerson implements Comparable<ComparablePerson> {

    private String name; private Integer age;

    public ComparablePerson(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public ComparablePerson() {}

    public int compareTo(ComparablePerson cp) {
        return this.age.compareTo(cp.age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    
    
}