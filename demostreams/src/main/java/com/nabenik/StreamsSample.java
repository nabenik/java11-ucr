package com.nabenik;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamsSample {

    public static void main(String args[]) {
        var cantidadPrimos = Stream.iterate(0, n -> n + 1)
            .limit(1_000_000)
            //.parallel() // Paralelizacionß
            .filter(StreamsSample::isPrime)
            //.peek(n -> System.out.print(n + " "))
            .count();

        System.out.println(cantidadPrimos);
    }

    public static boolean isPrime(int number) {
        if (number <=1 ) return false;

        return !IntStream
                .rangeClosed(2, number/2)
                .anyMatch(i -> number % i == 0);
    }
    
}